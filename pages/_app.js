import React from 'react'
import {ApolloProvider} from "@apollo/client";
import {SessionProvider} from "next-auth/react"
import Head from 'next/head'
import client from "../services/apollo/client";

import '../styles/icons.css'
import '../styles/main.scss'

export default function Nextra({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <SessionProvider session={session}>
      <ApolloProvider client={client}>
        <Head>
          <title>{"Clarck's Blog - Experiences & Solutions"}</title>
          <link
            rel="alternate"
            type="application/rss+xml"
            title="RSS"
            href="/feed.xml"
          />
        </Head>
        <Component {...pageProps} />
      </ApolloProvider>
    </SessionProvider>
  )
}



export async function getStaticProps() {
  return {
    props: {}
  }
}