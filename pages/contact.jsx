import React from "react";
import TopBar from "../comp/menus/topbar"
import ContactBox from "../comp/contactbox";


export default function Contact(props) {
  return (
    <div id="app">
      <TopBar />
      <div id="content">
        <section className="wrapper">
          <ContactBox />
        </section>
      </div>
    </div>
  )
}
