import React from 'react'
import TopBar from '../comp/menus/topbar'
import Presentation from '../comp/presentation/presentation'
import Index from '../comp/experiences'

export default function Home(props) {
  return (
    <div id="app">
      <TopBar />
      <div id="content">
        <section className="wrapper">
          <Presentation />
        </section>
        <section className="wrapper">
          <Index />
        </section>
      </div>
    </div>
  )
}
