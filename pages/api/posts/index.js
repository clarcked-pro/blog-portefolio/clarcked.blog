import {gql} from "@apollo/client"
import client from "../../../services/apollo/client"


export default async function post(req, res) {
    try {
        const {method, body: data} = req
        let resp;
        switch (method) {
            case "GET":
                console.log(data)
                res.status(200).json(data)
                break;
            case "POST":
                console.log("Post Data", data)
                resp = await client.mutate({
                    mutation: gql`
                        mutation CreatePost($post: PostInput!){
                            createPost(post: $post){
                                count
                                item{
                                    id
                                }
                            }
                        }
                    `,
                    variables: {
                        post: data
                    }
                })
                res.status(200).json(resp)
                break;
            case "PUT":
                console.log("Edit Post Data", data)
                resp = await client.mutate({
                    mutation: gql`
                        mutation UpdatePost($post: PostInput!){
                            updatePost(post: $post){
                                count
                                item{
                                    id
                                }
                            }
                        }
                    `,
                    variables: {
                        post: data
                    }
                })
                res.status(200).json(resp)
                break;
            default:
                res.status(404).json({message: `not found`})
                break;
        }
    } catch (error) {
        console.error("request", req.body)
        console.error("error", JSON.stringify(error))
        res.status(400).json({message: `bad request`})
    }
}