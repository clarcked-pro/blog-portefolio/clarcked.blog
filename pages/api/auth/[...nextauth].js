import NextAuth from "next-auth"
import GoogleProvider from "next-auth/providers/google"
import LinkedInProvider from "next-auth/providers/linkedin"

/**
 * This is an auth0 implementation powered by next-auth
 * - First get your application credentials from your providers [Google,LinkedIn]
 * - Setup the provider that you need
 * - Callback is where interesting will happen
 *  - signIn: allow authorization after the user signed up from the provider
 *  - jwt: is important to persist the OAuth access_token to the token right after signin
 *  - session: send properties to the client, like an access_token from a provider.
 *  - In the signIn if authorization is not granted we can return a URL to redirect to:
      like: return '/unauthorized'
 */
export default NextAuth({
  providers: [
    LinkedInProvider({
      clientId: process.env.LINKEDIN_CLIENT_ID,
      clientSecret: process.env.LINKEDIN_CLIENT_SECRET
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET
    })
  ],
  secret: process.env.JWT_SECRET,
  session: {
    strategy: "jwt",
    maxAge: 15 * 24 * 60 * 60, // 15 days
    updateAge: 24 * 60 * 60, // 24 hours
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    maxAge: 60 * 60 * 24 * 30,
    // async encode({ secret, token, maxAge }) {},
    // async decode({ secret, token }) {},
  },
  callbacks: {
    async signIn({ user, account, profile, email, credentials }) {
      const isAllowedToSignIn = user?.email === process.env.ADMIN_EMAIL
      if (isAllowedToSignIn) {
        return true
      } else {
        return false
      }
    },
    async jwt({ token, account }) {
      if (account) {
        token.accessToken = account.access_token
      }
      return token
    },
    async session({ session, token, user }) {
      session.accessToken = token.accessToken
      return session
    }
  },
  debug: true
})