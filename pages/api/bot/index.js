import {TelegramClient} from "messaging-api-telegram"


export default async function handler(req, res) {
  try {
    const teleg = new TelegramClient({
      accessToken: process.env.TELEGRAM_BOT_TOKEN,
    });
    const { method } = req
    switch (method) {
      case "POST":
        const data = req.body
        const resp = await teleg.sendMessage(
          process.env.TELEGRAM_CHAT_ID,
          JSON.stringify(data)
        )
        console.log(data)
        console.log(resp)
        res.status(200).json({})
        break;
      default:
        res.status(404).json({ message: `not found` })
        break;
    }
  } catch (error) {
    console.error("request", req.body)
    console.error("env", process.env.TELEGRAM_CHAT_ID)
    console.error("error", error.message)
  }
}