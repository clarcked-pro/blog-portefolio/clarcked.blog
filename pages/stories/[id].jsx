import React, {useEffect} from 'react'
import Head from 'next/head'
import {gql} from '@apollo/client'
import client from '../../services/apollo/client'
import TopBar from '../../comp/menus/topbar'
import Post from '../../comp/posts/post'

export default function Story(props) {
    const {post} = props
    useEffect(() => {
        console.log(post)
    }, [post])
    return (
        <div id="story">
            <Head>
                <title>{"Clarck's Blog - Index & Solutions"}</title>
            </Head>
            <TopBar/>
            <div id="content">
                <section className="story-wrapper wrapper">
                    <div className="story">
                        {post && <Post {...post} />}
                    </div>
                    <div className="story-side">
                        <div className="widget">
                            <div className="widget-content">
                                <form className="search-box" method="get" action="/blog">
                                    <div className="icon">
                                        <span className="icosearch"></span>
                                    </div>
                                    <div className="control">
                                        <input type="text" name="s" placeholder="search"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="widget">
                            <div className="widget-title">Recommended Topics</div>
                            <div className="widget-content tags">
                                <div className="tag">
                                    <span className="tag-name">Cloud Dev</span>
                                </div>
                                <div className="tag">
                                    <span className="tag-name">Nodejs</span>
                                </div>
                                <div className="tag">
                                    <span className="tag-name">AWS</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}


export async function getStaticProps({params}) {
    const {id} = params
    console.log("stories params:", params)
    return {
        props: {
            post: null
        }
    }
}

export async function getStaticPaths(context) {
    const paths = []
    console.log("stories context:", context)
    console.log("stories paths:", paths)
    return {
        paths,
        fallback: false
    }
}
