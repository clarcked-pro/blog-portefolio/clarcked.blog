import React from 'react'
import TopBar from '../../comp/menus/topbar'
import {PostLink} from '../../comp/posts/post'
import {excerpt} from '../../services/helpers'
import {getPublishedPosts} from "../../services/posts";
import {SearchWidget} from "../../comp/widgets/search";
import TagsWidget from "../../comp/widgets/tags";

export default function Stories(props) {
    const {posts} = props
    return (
        <div id="app">
            <TopBar/>
            <div id="content">
                <section className="wrapper rows gap-20">
                    <div className="col-10"/>
                    <div className="col-expand pad-40">
                        {posts?.map((post, key) => (
                            <PostLink key={key} showExcerpt={true} {...post} excerpt={excerpt(post)}/>
                        ))}
                    </div>
                    <div className="aside col-30 pad-40">
                        <SearchWidget/>
                        <TagsWidget/>
                    </div>
                </section>
            </div>
        </div>
    )
}

export async function getStaticProps() {
    const posts = await getPublishedPosts()
    return {
        props: {
            posts: posts?.items ?? []
        }
    }
}
