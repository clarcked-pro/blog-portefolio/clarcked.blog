import React, {useEffect, useState} from 'react'
import {getSession, signIn} from "next-auth/react"
import AdmLayout from "../../../../comp/layouts/adm";
import Editor from "../../../../comp/editor";
import FbLoader from "../../../../comp/loaders/fb";
import {createPost} from "../../../../services/posts";
import PostEntity from "../../../../model/entity/Post"
import EditorWidgets from "../../../../comp/editor/widgets";

export default function AddPost(props) {
    const {user} = props
    const [loading, setLoading] = useState(false)
    const [status, setStatus] = useState()
    const [STORAGE_KEY] = useState('@clarcked:editing-post')
    const publish = async () => {
        let post = sessionStorage.getItem(STORAGE_KEY)
        post = post ? JSON.parse(post) : post
        console.debug("saved post", post)
        if (post) {
            post = new PostEntity(post)
            setLoading(true)
            const data = await createPost(post)
            console.debug("data", data)
            setLoading(false)
        }
    }
    useEffect(() => {
        if (!user) signIn()
    })
    return (
        <AdmLayout user={user}>
            <section>
                <header className="rows pad-20">
                    <div className="col-expand">New story</div>
                    <div className="col-30">
                        {/*{status && <div className="status label">{status}</div>}*/}
                        <div className="actions">
                            <div className="action">
                                <button type="button" onClick={publish}>
                                    Publish
                                </button>
                            </div>
                        </div>
                    </div>
                </header>
                <div className="pad-40">
                    <Editor onStatusChange={setStatus}/>
                </div>
            </section>
            <aside>
                <EditorWidgets user={user}/>
            </aside>
            {loading && <FbLoader/>}
        </AdmLayout>
    )
}

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    return {props: {user: session?.user || null}}
}