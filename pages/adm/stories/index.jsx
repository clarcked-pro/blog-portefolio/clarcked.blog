import React, {useEffect, useState} from "react";
import Link from "next/link"
import {getSession, signIn} from "next-auth/react"
import AdmLayout from "../../../comp/layouts/adm";
import {PostLink} from "../../../comp/posts/post"
import {excerpt, sluggify} from "../../../services/helpers";
import StoryNav from "../../../comp/menus/story";
import {getPublishedPosts} from "../../../services/posts";

export default function Posts(props) {
    const {user, posts} = props
    const [STORAGE_KEY] = useState('@clarcked:editing-post')
    const onOpen = (post) => {
        sessionStorage.setItem(STORAGE_KEY, JSON.stringify(post))
        sessionStorage.setItem(`${STORAGE_KEY}:original`, JSON.stringify(post))
    }
    useEffect(() => {
        if (posts) console.log("posts", posts)
        if (!user) signIn().then()
    })
    return (
        <AdmLayout user={user}>
            <div>
                <header className="rows pad-20">
                    <div className="col-expand">Your stories</div>
                    <div className="col-30">
                        <div className="actions">
                            <div className="action">
                                <Link href="/adm/stories/add"><a>Create</a></Link>
                            </div>
                        </div>
                    </div>
                </header>
                <div>
                    <StoryNav/>
                    <div>
                        {posts?.map((post, key) => (
                            <PostLink
                                key={key}
                                href={{
                                    pathname: "/adm/stories/edit/[slug]",
                                    query: {slug: sluggify(post.title)}
                                }}
                                showExcerpt={true}
                                {...post}
                                excerpt={excerpt(post)}
                                onClick={() => onOpen(post)}/>
                        ))}
                    </div>
                </div>
            </div>
            <aside/>
        </AdmLayout>
    )
}

export async function getServerSideProps(ctx) {
    let session = await getSession(ctx)
    let data = await getPublishedPosts()
    console.debug("stories published:", JSON.stringify(data))
    return {
        props: {
            user: session?.user || null,
            posts: data?.posts?.items ?? []
        }
    }
}