import React, {useEffect, useState} from "react";
import Link from "next/link"
import {getSession, signIn} from "next-auth/react"
import AdmLayout from "../../../../comp/layouts/adm";
import StoryNav from "../../../../comp/menus/story";
import {excerpt, sluggify} from "../../../../services/helpers";
import {PostLink} from "../../../../comp/posts/post";

export default function Feedback(props) {
    const {user, posts} = props
    const [STORAGE_KEY] = useState('@clarcked:editing-post')
    const onOpen = (post) => {
        sessionStorage.setItem(STORAGE_KEY, JSON.stringify(post))
        sessionStorage.setItem(`${STORAGE_KEY}:original`, JSON.stringify(post))
    }
    useEffect(() => {
        if (!user) signIn().then()
    })
    return (
        <AdmLayout user={user}>
            <div>
                <header className="rows pad-20">
                    <div className="col-expand">Your stories</div>
                    <div className="col-30">
                        <div className="actions">
                            <div className="action">
                                <Link href="/adm/posts/add"><a>Create</a></Link>
                            </div>
                            <div className="action">
                                <button type="button" onClick={() => {}}>Import</button>
                            </div>
                        </div>
                    </div>
                </header>
                <div>
                    <StoryNav/>
                    <div>
                        {posts?.map((post, key) => (
                            <PostLink
                                key={key}
                                href={{
                                    pathname: "/adm/stories/edit/[slug]",
                                    query: {slug: sluggify(post.title)}
                                }}
                                showExcerpt={true}
                                {...post}
                                excerpt={excerpt(post)}
                                onClick={() => onOpen(post)}/>
                        ))}
                    </div>
                </div>
            </div>
            <aside className="adm-side"/>
        </AdmLayout>
    )
}

export async function getServerSideProps(ctx) {
    let session = await getSession(ctx)
    return {
        props: {
            user: session?.user || null,
            posts: []
        }
    }
}