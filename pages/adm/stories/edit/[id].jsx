import React, {useEffect, useState} from 'react'
import {getSession, signIn} from "next-auth/react"
import Editor from '../../../../comp/editor'
import AdmLayout from '../../../../comp/layouts/adm'
import {updatePost} from '../../../../services/posts'
import PostEntity from "../../../../model/entity/Post"
import FbLoader from "../../../../comp/loaders/fb"
import EditorWidgets from "../../../../comp/editor/widgets";

export default function EditPost(props) {
    const {user} = props
    const [loading, setLoading] = useState(false)
    const [status, setStatus] = useState()
    const [STORAGE_KEY] = useState('@clarcked:editing-post')
    const publish = async () => {
        let post = sessionStorage.getItem(STORAGE_KEY)
        post = post ? JSON.parse(post) : post
        if (post) {
            post = new PostEntity(post)
            setLoading(true)
            const data = await updatePost(post)
            setLoading(false)
        }
    }
    useEffect(() => {
        if (!user) signIn()
    })
    return (
        <AdmLayout user={user}>
            <section id="adm" className="posts adm-content">
                <header>
                    <div className="left">Edit story</div>
                    <div className="right">
                        {status && <div className="status label">{status}</div>}
                        <div className="actions">
                            <div className="action">
                                <button type="button" onClick={publish}>
                                    Publish
                                </button>
                            </div>
                        </div>
                    </div>
                </header>
                <Editor onStatusChange={setStatus}/>
            </section>
            <aside>
                <EditorWidgets user={user}/>
            </aside>
            {loading && <FbLoader/>}
        </AdmLayout>
    )
}

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    return {props: {user: session?.user || null}}
}