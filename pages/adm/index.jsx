import React, {useEffect} from "react";
import {getSession, signIn} from "next-auth/react"
import AdmLayout from "../../comp/layouts/adm";


export default function Adm(props) {
    const {user} = props
    useEffect(() => {
        if (!user) signIn().then()
    })
    if (user) {
        return (
            <AdmLayout user={user}>
                <div className="adm">
                </div>
                <aside className="adm-side">
                </aside>
            </AdmLayout>
        )
    }
    return <></>
}


export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    return {props: {user: session?.user || null}}
}