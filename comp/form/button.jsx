import React from "react"
import Field from "./field";
import style from "./style.module.scss"

export default function Button(props){
    return (
        <Field
            id={props.id}
            type="submit"
            label={props.label}
            className={[style['submit'], props.className].join(" ")}
        />
    )
}