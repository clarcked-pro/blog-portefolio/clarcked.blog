import React from 'react'
import style from "./style.module.scss"

export default function Field(props) {
    const {
        id,
        type,
        name,
        label,
        className,
        defaultvalue,
        register,
        required,
        errors
    } = props
    return (
        <div className={[style['field'], className].join(' ')}>
            {type !== 'submit' && (
                <label htmlFor={id} className={style["label"]}>
                    {label || name}
                </label>
            )}
            <div className={[style['input'], type].join(' ')}>
                {type === 'textarea' ? (
                    <textarea
                        id={id}
                        defaultValue={defaultvalue}
                        {...register(name, {required: required | false})}
                    />
                ) : type === 'submit' ? (
                    <input id={id} type={type} value={label}/>
                ) : (
                    <input
                        id={id}
                        defaultValue={defaultvalue}
                        {...register(name, {required: required | false})}
                    />
                )}
                <div className={style["messages"]}>
                    {errors && errors[`${name}Required`] && (
                        <span>This field is required</span>
                    )}
                </div>
            </div>
        </div>
    )
}
