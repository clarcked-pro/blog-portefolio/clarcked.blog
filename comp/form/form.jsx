import React from 'react'
import {useForm} from 'react-hook-form'
import style from "./style.module.scss"

export default function Form(props) {
  const { id, className, children, onSubmit, onWatch, watchList } = props
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm()
  let fields = React.Children.toArray(children)
  if (fields.length === 1) {
    fields = React.cloneElement(fields[0], { errors, register })
  } else if (fields.length > 0) {
    fields = fields.map(
      (field) => (fields = React.cloneElement(field, { errors, register }))
    )
  }
  React.useEffect(() => {
    const watchFields = Array.isArray(watchList) ? watch(watchList) : watch()
    onWatch(watchFields)
  })
  return (
    <form
      id={id}
      className={[style['form'], className].join(' ')}
      onSubmit={handleSubmit(onSubmit)}
    >
      {fields}
    </form>
  )
}
