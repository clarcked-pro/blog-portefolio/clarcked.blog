import React from "react"
import style from "./style.module.scss"

export default function FbLoader(props) {
  return <div className={style.loader}><div className={style["lds-facebook"]}><div></div><div></div><div></div></div></div>
}