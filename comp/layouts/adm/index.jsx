import React from 'react'
import AdmMenu from "../../menus/adm";
import style from "./style.module.scss"

export default function AdmLayout(props) {
    const {children, user} = props
    return (
        <div id={style["layout-adm"]} className={[style["layout"], style["adm"]].join(" ")}>
            <AdmMenu user={user}/>
            <section>{children}</section>
        </div>
    )
}
