import React from "react"
import style from "./style.module.scss"


export function SearchForm(props) {
    return (
        <form className={style["search"]} method="get" action="/blog">
            <div className={style["icon"]}>
                <span className="icosearch"></span>
            </div>
            <div className={style["control"]}>
                <input type="text" name="s" placeholder="search"/>
            </div>
        </form>
    )
}

export function SearchWidget(props) {
    return (
        <div className="widget">
            <div className="widget-content">
                <SearchForm/>
            </div>
        </div>
    )
}
