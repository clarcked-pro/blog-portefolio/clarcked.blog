import React from 'react'
import style from "./style.module.scss"

export default function TagsWidget(props){
    return(
        <div className="widget">
            <div className="widget-title">Recommended Topics</div>
            <div className="widget-content tags">
                <div className="tag">
                    <span className="tag-name">Cloud Dev</span>
                </div>
            </div>
        </div>
    )
}