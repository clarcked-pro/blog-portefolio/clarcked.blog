import React from 'react'
import style from './style.module.scss'

const BlogPresentation = (props) => {
  return (
    <div id={style["presentation"]}>
      <div className={style["bloginfo"]}>
        <blockquote>
          Clarcked's blog stands for sharing my experiences, solutions, and
          making connections. Please feel free to leave your feedback, open a
          discussion.
        </blockquote>
      </div>
    </div>
  )
}

export default BlogPresentation
