import React, {useEffect, useRef} from 'react';
import {gsap} from "gsap";
import {TextPlugin} from "gsap/dist/TextPlugin";
import style from "./style.module.scss";


const Presentation = (props) => {
  const contentRef = useRef();
  useEffect(() => {
    gsap.registerPlugin(TextPlugin);
    gsap.to(contentRef.current, 7, { text: " How can I help you to improve your cloud base product, your web app or your mobile app?", ease: "none" });
  });
  return (
    <div id={style.presentation}>
      <div className={style.infos}>
        <div className={style.myname}>
          Clarck J. J. MONCLAIR
        </div>
        <div className={style.details}>
          <div className={style.detail}>// About me</div>
          <div className={style.title}>
            Exprerienced Cloud Engineer, UX/UI Designer
          </div>
          <div className={style.content} ref={contentRef}></div>
        </div>
      </div>
      <div className={style.avatar}>
        <img src="./images/avatar-clarck.png" alt="avatar" />
      </div>
    </div>
  )
}

export default Presentation