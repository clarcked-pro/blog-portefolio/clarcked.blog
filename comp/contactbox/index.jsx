import React from 'react'
import {Field, Form} from '../form'
import Sent from './sent'
import {send_telegram} from '../../services/telegram'
import style from './style.module.scss'
import {Button} from "../form";

export default function ContactBox(props) {
    const [submitable, setSubmitable] = React.useState(false)
    const [sent, setSent] = React.useState(false)
    const [loading, setLoading] = React.useState(false)

    const watch = (data) => {
        submitability(data)
    }
    const submit = async (data) => {
        setLoading(true)
        const resp = await send_telegram(data)
        setSent(true)
        setLoading(false)
    }
    const submitability = ({email, name, message}) => {
        const isOk = !!(email && name && message)
        setSubmitable(isOk)
    }
    return (
        <div id={style["contact"]}>
            <section className={[style["heading"], "wrapper"].join(" ")}>
                <h1>...want to contact me?</h1>
                <h2>let's get in touch..!</h2>
            </section>
            <section className={["wrapper"].join(" ")}>
                <div className={style["details"]}>
                    <p>
                        This form aside will broadcast your message to my{' '}
                        <em>#slackbot</em>, <em>#telegram</em> and <em>my email box</em>,
                        that is make it the fastest and secure way to contact me. And if
                        your are not a spam, if you correctly identify yourself you will
                        probably receive a feedback in a very short time.
                    </p>
                    <div className={style["socials"]}>
                        <div className={style["social_title"]}>Or you can find me on:</div>
                        <div className={style["social_links"]}>
                            <div className={[style["social_link"], style["linkedin"]].join(' ')}>
                                <a
                                    href="https://linkedin.com/in/clarck-monclair"
                                    target="blank"
                                >
                                    <span className="icolinkedin"/>
                                </a>
                            </div>
                            <div className={[style["social_link"], style["twitter"]].join(' ')}>
                                <a href="https://twitter.com/@_mclarck" target="blank">
                                    <span className="icotwitter"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {sent ? (
                    <Sent/>
                ) : (
                    <Form
                        className={style["contact_form"]}
                        onSubmit={submit}
                        onWatch={watch}
                    >
                        <Field id="name" name="name" label="Name" />
                        <Field id="email" name="email" label="Email" />
                        <Field
                            id="message"
                            name="message"
                            type="textarea"
                            label="Your message"
                        />
                        {submitable && (
                            <Button
                                id="submit-contact"
                                type="submit"
                                label={loading ? 'Sending...' : 'Send Message'}
                            />
                        )}
                    </Form>
                )}
            </section>
        </div>
    )
}
