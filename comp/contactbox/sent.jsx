import React from 'react'
import style from './style.module.scss'

export default function Sent(props) {
  return (
    <div className={style.sent}>
      <div className={style.sent_msg}>
        <span className={['icoinbox-check', style.icon].join(' ')}></span>
        <br />
        <span>
          Greats! your message has been sent !!! <br />
          Thank you for contacting me...
        </span>
        <br />
        <a href="/contact">
          <span className={['icoarrow-thin-left', style.icon].join(' ')}></span>
        </a>
      </div>
    </div>
  )
}
