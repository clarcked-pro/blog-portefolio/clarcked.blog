import React, {createRef, useEffect, useState} from "react";

export default function useKeyEvent(props) {
  const { onSave, onStatusChange } = props
  const [, refresh] = useState(0)
  const [loading, setLoading] = useState(false)
  const [editing, setEditing] = useState(false)
  const [touched, setTouched] = useState(false)
  const [status, setStatus] = useState("")
  const [STORAGE_KEY] = useState("@clarcked:editing-post")
  // refs
  const editoref = createRef()
  // methods
  const changeStatus = (arg) => {
    setStatus(n => n = arg)
    if (typeof onStatusChange === "function") onStatusChange(arg)
  }
  const addNode = (type = "p", children = "", props) => {
    const root = editoref.current
    if (root) {
      const child = document.createElement(type)
      child.innerHTML = children
      root.appendChild(child)
      root.focus()
    }
  }
  const getContent = () => {
    const root = editoref.current;
    if (root) {
      const content = root.innerHTML
      const title = root.getElementsByTagName("h1")
      const post = {
        title: title[0] ? title[0].innerText : "",
        content: content
      }
      return post
    }
  }
  const setContent = (content) => {
    if (!content) return
    const root = editoref.current;
    if (root) {
      root.innerHTML = content
    }
  }
  const getSavedPost = () => {
    let post = sessionStorage.getItem(STORAGE_KEY)
    post = post ? JSON.parse(post) : {}
    return post
  }
  const getSavedContent = () => {
    let post = getSavedPost()
    return post.content
  }
  const getSelection = () => {
    const obj = window.getSelection()
    const range = obj.getRangeAt(0)
    console.log("selected range:", range)
  }
  const handleState = () => {
    const post = getContent()
    const savedContent = getSavedContent()
    function setDirty() {
      console.log("editor is dirty")
      setEditing(true)
      setTouched(true)
      changeStatus("editing")
    }
    if (savedContent) {
      console.log("we have saved content")
      if (post && post.content != savedContent) {
        setDirty()
      }
    } else {
      if (savedContent && savedContent != "<h1>Title</h1>") {
        setDirty()
      }
    }
  }
  const save = (post) => {
    try {
      const oldPost = getSavedPost()
      sessionStorage.setItem(STORAGE_KEY, JSON.stringify({ ...oldPost, ...post }))
      if (typeof onSave === "function") onSave(post)
    } catch (err) {
      changeStatus("not saved")
      console.warn("Something wrong happened while saving this post.")
      console.warn(err.message)
    }
  }
  // combined key events
  const onCtrlS = (ev) => {
    const post = getContent()
    if (post) {
      console.info("post:", post)
      console.info('saving...')
      changeStatus("saving")
      save(post)
      changeStatus("saved")
    }
    ev?.preventDefault()
  }
  const onCtrlP = (ev) => {
    addNode("p", "Tell your stories")
    ev?.preventDefault()
  }
  const onCtrlH = (ev) => {
    addNode("h2", "Your subtitle here")
    ev?.preventDefault()
  }
  const onCtrlT = (ev) => {
    addNode("h1", "Title")
    ev?.preventDefault()
  }
  const onCtrlShiftC = (ev) => {
    addNode("code", "// you code sample")
    ev?.preventDefault()
  }
  const onCtrlB = (ev) => {
    const sel = getSelection()
    ev?.preventDefault()
  }
  // basics key events
  const onClick = (ev) => { }
  const onFocus = (ev) => { }
  const onBlur = (ev) => { }
  const onKeyPress = (ev) => { }
  const onKeyDown = (ev) => {
    const { altKey, ctrlKey, metaKey, shiftKey, key, preventDefault } = ev;
    handleState()
    if (ctrlKey) {
      switch (key) {
        case "s":
          onCtrlS(ev)
          break;
        case "b":
          onCtrlB(ev)
          break;
        case "h":
          onCtrlH(ev)
          break;
        case "t":
          onCtrlT(ev)
          break;
        case "p":
          onCtrlP(ev)
          break;
        case "C":
          onCtrlShiftC(ev)
          break;
        default:
          break;
      }
    } else {
      switch (key) {
        default:
          break;
      }
    }
  }
  // react events
  useEffect(() => {
  }, [])
  // -----
  return {
    editorState: editing ? "editing" : touched ? "touched" : loading ? "loading" : "untouched",
    editing: editing,
    touched: touched,
    loading: loading,
    status: status,
    editoref: editoref,
    add: addNode,
    setContent: setContent,
    content: getSavedContent,
    keyPress: onKeyPress,
    keyDown: onKeyDown,
    ctrlP: onCtrlP,
    ctrlS: onCtrlS,
    blur: onBlur,
    focus: onFocus,
    click: onClick,
    refresh: () => refresh(n => n = !n)
  }
}