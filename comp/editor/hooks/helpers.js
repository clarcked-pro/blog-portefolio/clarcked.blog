import React from "react";

export default function useHelpers(props) {
  const addClass = (target, className) => {
    if (target?.classList?.contains(className)) {
      target?.classList?.toggle(className)
    }
  }
  const removeClass = (target, className) => {
    if (target?.classList?.contains(className)) {
      target?.classList?.remove(className)
    }
  }
  return {
    addClass,
    removeClass
  }
}