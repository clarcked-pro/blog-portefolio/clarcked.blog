import React, {useEffect} from 'react'
import {useKeyEvent} from './hooks'
import style from "./style.module.scss"

/**
 * Editor Component
 * This component stands for editing a text dedicated to be published
 * in a blog.
 * features:
 * - WYSWYG use of editable content html
 * - After edition is done execute a callback
 *   by passing the html content of the container as an argument
 * - KeyEvent:
 *  - Global
 *    - Escape : cancel any current operation
 *    - Ctrl+S : execute the callback
 *  - Local
 *    - ArrowUp/ArrowDown : Navigate to next component
 *    - Alt+Up/Alt+Down : Move the component top/bottom
 *    - Ctrl+P : add a new <p></p>
 *    - Ctrl+H : add a new <h*></h*>
 *    - Ctrl+B : wrap the selection with <b></b>
 *    - Ctrl+I : wrap the selection with <i></i>
 *    - Ctrl+Q : wrap the selection with <quote></quote>
 *    - Ctrl+L : wrap the selection with <a></a>, prompt to set attributes
 *    - Ctrl+Enter : new editing block
 *
 * @param {*} props
 * @returns
 */
export default function Editor(props) {
    const {
        editorState,
        add,
        content,
        setContent,
        editoref,
        keyPress,
        keyDown,
        ctrlP,
        ctrlS
    } = useKeyEvent(props)
    useEffect(() => {
        add('h1', 'Title')
        ctrlP()
        setContent(content())
        setInterval(() => {
            ctrlS()
        }, 10000)
        return () => {
            clearInterval()
        }
    }, [])
    return (
        <section
            ref={editoref}
            id="editor"
            className={['post', editorState].join(' ')}
            onKeyPress={keyPress}
            onKeyDown={keyDown}
            suppressContentEditableWarning
            contentEditable
        />
    )
}
