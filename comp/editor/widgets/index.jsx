import React from "react"
import style from "./style.module.scss"

export default function EditorWidgets(props) {
    return (
        <div className={[style["editor-widgets"]].join(" ")}>
            <div className="widget">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet aspernatur consequatur culpa,
                dolorum eaque fugit, incidunt laborum maxime modi odit officiis omnis porro ratione similique soluta
                ullam veniam voluptas.
            </div>
        </div>
    )
}