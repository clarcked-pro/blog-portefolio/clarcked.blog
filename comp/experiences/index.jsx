import React from 'react';
import style from "./style.module.scss";


const Index = (props) => {
  return (
    <div id={style["experiences"]}>
      <div className={style["title"]}>Working Experiences</div>
      <div className={style["items"]}>
        <div className={style["item"]}>
          <img src="/images/ibm.svg" alt="IBM" width={90} />
        </div>
        <div className={style["item"]}>
          <img src="/images/spv.png" alt="SuperVielle" width={150} />
        </div>
      </div>
    </div>
  )
}

export default Index