import React from "react";
import ActiveLink from "../links/activelink";
import style from "./style.module.scss"


export default function StoryNav(props) {
  return (
    <nav className={[style["story-nav"]].join(" ")}>
      <ul>
        <li>
          <ActiveLink href="/adm/stories" activeClassName={style["active"]}>
            <a>Published</a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink href="/adm/stories/drafts" activeClassName={style["active"]}>
            <a>Drafts</a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink href="/adm/stories/feedbacks" activeClassName={style["active"]}>
            <a>Feedbacks</a>
          </ActiveLink>
        </li>
      </ul>
    </nav>
  )
}