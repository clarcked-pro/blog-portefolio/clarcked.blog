import React from "react";
import Link from "next/link";
import ActiveLink from "../links/activelink";
import style from "./style.module.scss"


export default function TopBar(props) {
  return (
    <nav id={style["topBar"]} className={["navs",style["topBar"]].join(" ")}>
      <ul className={style["left"]}>
        <li>
          <Link href="/">
            <a id={style["brand"]}>
              <div id={style["brand-logo"]}>
                <img src='/images/clarcked.png' alt="clarcked-logo" width={25} height={25} />
              </div>
              <div id={style["brand-name"]}>Clarcked</div>
            </a>
          </Link>
        </li>
      </ul>
      <ul className={style["right"]}>
        <li>
          <ActiveLink href="/stories" >
            <a id={style["blog-link"]} >Blog</a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink href="/" >
            <a id={style["skill-link"]} >Skills</a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink href="/contact" >
            <a id={style["contact-link"]}>Contact</a>
          </ActiveLink>
        </li>
      </ul>
    </nav>
  )
}