import React from 'react'
import Link from 'next/link'
import {signOut} from "next-auth/react"
import Avatar from "../../avatar";
import style from "./style.module.scss"

export default function AdmMenu(props) {
    const {user} = props
    return (
        <nav className={[style["adm-nav"], " "].join(" ")}>
            <ul>
                <li>
                    <Link href="/adm">
                        <a>
                            <img src="/images/clarcked.png" width="25"/>
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        <a>
                            <span className="icolink"></span>
                        </a>
                    </Link>
                </li>
            </ul>
            <ul>
                <li>
                    <Link href="/adm/stories/">
                        <a>
                            <div className="icon">
                                <span className="iconews-paper"></span>
                            </div>
                        </a>
                    </Link>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="#">
                        <div className="icon">
                            {user ?
                                <Avatar src={user?.image}/> :
                                <span className="icouser"></span>}
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" onClick={signOut}>
                        <div className="icon">
                            <span className="icostand-by"></span>
                        </div>
                    </a>
                </li>
            </ul>
        </nav>
    )
}
