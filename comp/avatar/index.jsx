import React from "react"
import style from "./style.module.scss"

export default function Avatar(props){
    return(<div className={style["avatar"]}><img src={props.src} {...props}  /></div>)
}