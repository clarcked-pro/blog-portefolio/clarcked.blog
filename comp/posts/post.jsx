import React from 'react'
import moment from "moment"
import Link from "next/link"
import {readtime, sluggify} from "../../services/helpers"

const Post = ({ createdAt, title, content, excerpt, tags, category, author, lang, cover, showExcerpt, notlink }) => {
  return (
    <article className="post">
      <header>
        <div className="left">
          <div className="avatar">
            <img
              src="https://miro.medium.com/fit/c/32/32/0*qcyI6dTMxtqOlMV6.jpg"
              alt="clarck"
            />
          </div>
          <div className="author">{author?.username}</div>
          <div className="category">
            <span>in</span>{' '}
            <span className="category-name">{category?.name}</span>
          </div>
          <div className="date">
            {moment(createdAt).locale(lang ?? "en").format("ll")}
          </div>
        </div>
      </header>
      {showExcerpt && (<><h1>{title}</h1></>)}
      <div className="content-wrapper">
        <div className="content" dangerouslySetInnerHTML={{ __html: showExcerpt ? excerpt : content }}></div>
        {cover &&
          <div className="img-cover">
            <img src={cover.link} alt="post-image" />
          </div>
        }
      </div>
      <footer>
        <div className="left">
          <div className="tags tags-o">
            {tags?.map((tag, key) => (
              <div key={key} className="tag">#{tag.name}</div>
            ))}
          </div>
          <div className="read-time">{readtime(content)}</div>
        </div>
        <div className="right"></div>
      </footer>
    </article>
  )
}



export const PostLink = (props) => (
  <Link
    href={props.href ?? {
      pathname: `/story/[slug]`,
      query: { slug: sluggify(props.title) }
    }}
    locale={props.lang} >
    <a className="post-link" onClick={props.onClick}>
      <Post {...props} />
    </a>
  </Link>)

export default Post
