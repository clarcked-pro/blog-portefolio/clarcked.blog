import {ApolloClient, createHttpLink, InMemoryCache} from "@apollo/client";

const client = new ApolloClient({
  link: createHttpLink({
    uri: process.env.GRAPHQL_ENDPOINT,
    headers: {
      "x-api-key": process.env.GRAPHQL_APIKEY
    }
  }),
  cache: new InMemoryCache(),
});

export default client;