export async function send_telegram(msg) {
  const options = {
    method: 'POST',
    body: JSON.stringify(msg),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  const resp = await fetch("/api/bot", options)
  return resp
}