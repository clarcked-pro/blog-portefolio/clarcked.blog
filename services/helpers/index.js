export const sluggify = (arg) => {
  if (!arg) return
  let slug = arg.trim().split(/\s{1,}/).join("-").replace(/[\.\,\']/, "").replace(/[']/, "-").toLowerCase();
  return slug
}

export const strip = (arg) => arg.replace(/(<([^>]+)>)/ig, "")

export const readtime = (arg) => {
  if (typeof arg != "string") return
  const content = strip(arg)
  const words = content.split(" ")
  const numberOfWords = words.length
  // an adult can read 220-350 words per min
  const WPM = 220
  const minutes = Math.round(numberOfWords / WPM)
  // console.log("content", content)
  // console.log("words", words)
  // console.log("number of words", numberOfWords)
  return `${minutes | 1} min`
}


export const excerpt = (post) => {
  let content = post.excerpt ?? strip(post.content)
  content = content.slice(0, 280)
  return `${content} ...`
}