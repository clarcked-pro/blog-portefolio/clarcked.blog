import {gql} from "@apollo/client";

export const POST_PUBLISHED = gql`
    query GetPublishedPosts($search: SearchInput){
        posts(search: $search) {
            count
            items{
                id
                title
                content
                postStatus
                tags{
                    name
                    slug
                }
                category{
                    name
                    slug
                }
                author{
                    username
                    image
                    email
                }
            }
        }
    }
`

export const ALL_POSTS = gql`
    query GetPosts($search: SearchInput){
        posts(search: $search) {
            count
            items{
                id
                meta
                title
                content
                postStatus
                author {
                    username
                    image
                    email
                }
            }
        }
    }
`