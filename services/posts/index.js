import client from "../apollo/client";
import {ALL_POSTS, POST_PUBLISHED} from "./queries";

export async function fetchPosts(query = ALL_POSTS, options = {search: {}}) {
    try {
        const {data} = await client.query({
            query: query,
            variables: options
        })
        return data
    } catch (error) {
        console.warn("WARNING:", JSON.stringify(error))
    }
}

export async function getPublishedPosts() {
    return await fetchPosts(POST_PUBLISHED, {search: {keys: [{name: "postStatus", value: "published"}]}})
}

export async function createPost(data) {
    try {
        const options = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        return await fetch("/api/posts", options)
    } catch (error) {
        console.warn("WARNING:", JSON.stringify(error))
    }
}

export async function updatePost(data) {
    try {
        const options = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        return await fetch("/api/posts", options)
    } catch (error) {
        console.warn("WARNING:", JSON.stringify(error))
    }
}