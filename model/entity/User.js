import moment from "moment";

export default class User {
    id
    meta
    createdAt
    updatedAt
    username
    fullname
    userStatus
    email
    name
    image
    role
    password
    constructor({id, createdAt, updatedAt, meta, username, fullname, email, userStatus, name, image, role, password}) {
        this.createdAt = createdAt ?? moment().format("YYYY/MM/DD")
        this.updatedAt = updatedAt ?? moment().format("YYYY/MM/DD")
        this.meta = meta ?? this.createdAt
        this.userStatus = userStatus ?? "active"
        this.username = username
        this.fullname = fullname
        this.email = email
        this.name = name
        this.image = image
        this.role = role
        this.password = password
        this.id = id ? `${id}` : `${this.username}`
    }
}