import moment from "moment"
import Tag from "./Tag"
import User from "./User"
import Category from "./Category"
import Comment from "./Comment";
import {sluggify} from "../../services/helpers";

export default class Post {
    id
    meta
    createdAt
    updatedAt
    title
    content
    excerpt
    lang
    postStatus
    postType
    author
    category
    tags = []
    comments = []

    constructor({
                    id,
                    meta,
                    createdAt,
                    updatedAt,
                    title,
                    content,
                    excerpt,
                    lang,
                    postStatus,
                    author,
                    category,
                    tags,
                    comments,
                    postType
                }) {
        this.createdAt = createdAt ?? moment().format("YYYY/MM/DD")
        this.updatedAt = updatedAt ?? moment().format("YYYY/MM/DD")
        this.meta = meta ?? this.createdAt
        this.title = title
        this.content = content
        this.excerpt = excerpt
        this.lang = lang ?? "en"
        this.postStatus = postStatus ?? "pending"
        this.author = author ? new User(author) : new User({username: "clarcked"})
        this.category = category ? new Category(category) : new Category({name: "Uncategorized"})
        this.tags = tags ? tags.map((tag) => new Tag(tag)) : [new Tag({name: "Untagged"})]
        this.comments = comments ? comments.map(c => new Comment(c)) : []
        this.postType = postType ?? "POST"
        this.id = id ? `${id}` : `${sluggify(title)}`
    }
}