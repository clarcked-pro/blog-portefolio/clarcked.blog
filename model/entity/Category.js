import {sluggify} from "../../services/helpers"
import moment from "moment";

export default class Category {
    id
    createdAt
    updatedAt
    meta
    name
    slug

    constructor({id, createdAt, updatedAt, name, meta, slug}) {
        this.createdAt = createdAt ?? moment().format("YYYY/MM/DD")
        this.updatedAt = updatedAt ?? moment().format("YYYY/MM/DD")
        this.name = name
        this.meta = meta ?? this.createdAt
        this.slug = slug ?? sluggify(name)
        this.id = id ? `${id}` : `${this.slug}`
    }
}