import moment from "moment"
import {sluggify} from "../../services/helpers";

export default class Comment {
    id
    meta
    createdAt
    modifiedAt
    content
    commentStatus
    author

    constructor({id, meta, createdAt, modifiedAt, content, author, commentStatus}) {
        this.createdAt = createdAt ?? moment().format("YYYY/MM/DD")
        this.modifiedAt = modifiedAt ?? moment().format("YYYY/MM/DD")
        this.meta = meta ?? this.createdAt
        this.content = content
        this.author = author
        this.commentStatus = commentStatus ?? "inactive"
        this.id = id ? `${id}` : `${sluggify(this.createdAt)}`
    }
}