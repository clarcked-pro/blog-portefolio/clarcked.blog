import moment from "moment";
import {sluggify} from "../../services/helpers"

export default class Tag {
  id
  createdAt
  updatedAt
  meta
  name
  slug
  constructor({ id, createdAt, updatedAt, meta, name, slug }) {
    this.createdAt = createdAt ?? moment().format("YYYY/MM/DD")
    this.updatedAt = updatedAt ?? moment().format("YYYY/MM/DD")
    this.meta = meta ?? this.createdAt
    this.name = name
    this.slug = slug ?? sluggify(name)
    this.id = id ? `${id}`: `${this.slug}`
  }
}